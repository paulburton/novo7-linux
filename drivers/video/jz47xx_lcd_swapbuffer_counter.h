
#ifndef JZ47XX_LCD_SWAPBUFFER_COUNTER_H
#define JZ47XX_LCD_SWAPBUFFER_COUNTER_H

//#define JZ47XX_LCD_SWAPBUFFER_GET_LATEST_FPS

extern int jz47xx_lcd_swapbuffer_count_increase(int n);
extern int jz47xx_lcd_swapbuffer_get_latest_fps(int latest);


#endif // JZ47XX_LCD_SWAPBUFFER_COUNTER_H
